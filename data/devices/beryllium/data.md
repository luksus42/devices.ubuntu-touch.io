---
name: "Xiaomi Poco F1"
comment: "community device"
deviceType: "phone"
description: "As hardware enthusiasts, you likely know how tough it is to get access to new, hyped hardware. When something like Xiaomi's POCO F1 surfaces, the intrigue definitely mounts. The POCO F1 (or sporting comparable internals) can still be expected to be difficult to get your hands on due to the effortless desire the covers to get there hands on it. The Acme Labs POCO F1 is one of the few devices still sporting hardware. POCO F1 you got an octa-core processor with up to 8 GB RAM, dual rear fingerprint sensor, 16 MP rear camera with support for Netflix's 120fps stabilization"
price:
  avg: 250

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x 2.8 GHz Kryo 385 Gold & 4x 1.8 GHz Kryo 385 Silver)"
  - id: "chipset"
    value: "Qualcomm SDM845 Snapdragon 845"
  - id: "gpu"
    value: "Adreno 630"
  - id: "rom"
    value: "64 GB, 128 GB, 256 GB"
  - id: "ram"
    value: "6 GB, 8 GB"
  - id: "android"
    value: "Android 8.1 (Oreo), upgradable to Android 10.0"
  - id: "battery"
    value: "Li-Po 4000 mAh, non-removable"
  - id: "display"
    value: "6.18 inches, 96.2 cm2 (~82.2% screen-to-body ratio), 1080 x 2246 pixels, 18.7:9 ratio (~403 ppi density)"
  - id: "rearCamera"
    value: '12 MP, f/1.9, 1/2.55", 1.4µm, dual pixel PDAF, 5 MP, f/2.0, (depth)'
  - id: "frontCamera"
    value: '20 MP, f/2.0, (wide), 1/3", 0.9µm'
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "155.5 x 75.3 x 8.8 mm (6.12 x 2.96 x 0.35 in)"
  - id: "weight"
    value: "182 g (6.42 oz)"

contributors:
  - name: "Joel Selvaraj"
    forum: "https://forums.ubports.com/user/joels"

sources:
  portType: "community"
  portPath: "android9"
  deviceGroup: "xiaomi-poco-f1"
  deviceSource: "xiaomi-beryllium"
  kernelSource: "kernel-xiaomi-beryllium"
  showDevicePipelines: true

communityHelp:
  - name: "Ubuntu Touch for Poco F1"
    link: "https://t.me/ubportsforpocof1"
  - name: "Matrix Space - Linux on Poco F1"
    link: "https://matrix.to/#/#linuxonpocof1:matrix.org"
---
