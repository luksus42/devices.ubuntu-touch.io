---
name: "OnePlus Nord N10 5G"
comment: "community device"
description: "The OnePlus Nord N10 is a good level Android smartphone, great for photos, that can satisfy even the most demanding of users."
deviceType: "phone"
price:
  avg: 217,25

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm SM6350 Snapdragon 690 5G (8 nm)"
  - id: "gpu"
    value: "Adreno 619L"
  - id: "rom"
    value: "128GB"
  - id: "ram"
    value: "6GB"
  - id: "android"
    value: "Android 10.0"
  - id: "battery"
    value: "4300 mAh"
  - id: "display"
    value: "1080 x 2400 pixels, 20:9 ratio 406 ppi density"
  - id: "rearCamera"
    value: "64 MP"
  - id: "frontCamera"
    value: "16 MP, f/2.1"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "163 x 74.7 x 9 mm (6.42 x 2.94 x 0.35 in)"
  - id: "weight"
    value: "190 g (6.70 oz)"
  - id: "releaseDate"
    value: "Nov 21 2020"

sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "oneplus-nord-n10"
  deviceSource: "oneplus-nord-n10"
  kernelSource: "kernel-oneplus-sm6350"

communityHelp:
  - name: "Telegram - @rubencarneiro"
    link: "https://t.me/rubencarneiro"

contributors:
  - name: TheKit
    forum: https://forums.ubports.com/user/thekit
    role: "Developer"
  - name: "Rúben Carneiro"
    role: "Maintainer"
    forum: "https://gitlab.com/rubencarneiro"
    photo: "https://gitlab.com/uploads/-/system/user/avatar/2386810/avatar.png"
---
