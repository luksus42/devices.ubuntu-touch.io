---
name: "Lenovo Tab M10 (2nd Gen) WiFi "
comment: "community device"
description: "The Lenovo Tab M10 HD is a high-powered 10.1 (25.65 cm) 2nd generation HD tablet with a premium metallic design. It delivers fantastic sound and has tons of features for the whole family."
deviceType: "tablet"
price:
  avg: 140

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Mediatek MT6762 Helio P22T (12 nm)"
  - id: "gpu"
    value: "PowerVR GE8320"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "3/4GB"
  - id: "android"
    value: "Android 10.0"
  - id: "battery"
    value: "5000 mAh"
  - id: "display"
    value: "800 x 1280 pixels, 16:10 ratio (~149 ppi density)"
  - id: "rearCamera"
    value: "8MP"
  - id: "frontCamera"
    value: "5MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "10.1 inches, 295.8 cm2 (~82.0% screen-to-body ratio)"
  - id: "weight"
    value: "420 g"
  - id: "releaseDate"
    value: "Nov 2020"

sources:
  portType: "community"
  portPath: "android11"
  deviceGroup: "lenovo-tab-m10-hd"
  deviceSource: "lenovo-m10-hd"
  kernelSource: "kernel-lenovo-amar"

communityHelp:
  - name: "Telegram - @rubencarneiro"
    link: "https://t.me/rubencarneiro"

contributors:
  - name: "Rúben Carneiro"
    role: "Maintainer"
    forum: "https://gitlab.com/rubencarneiro"
    photo: "https://gitlab.com/uploads/-/system/user/avatar/2386810/avatar.png"
---
