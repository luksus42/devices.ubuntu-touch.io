---
variantOf: "beyond1lte"
name: "Samsung Galaxy S10+ (Exynos)"

deviceInfo:
  - id: "rom"
    value: "128/512/1024 GB"
  - id: "ram"
    value: "8/12 GB"
  - id: "battery"
    value: "4100 mAh"
  - id: "display"
    value: "1440x3040 pixels, 6.4 in"
  - id: "dimensions"
    value: "157.6 x 74.1 x 7.8 mm (6.20 x 2.92 x 0.31 in)"
  - id: "weight"
    value: "175g"
---
