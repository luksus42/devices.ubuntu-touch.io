---
name: "OnePlus Nord N100"
comment: "community device"
description: "The OnePlus Nord N100 delivers good user experience with a long lasting battery of 5,000 mAh, and a display of 6.52."
deviceType: "phone"
price:
  avg: 120

deviceInfo:
  - id: "cpu"
    value: "Octa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm SM4250 Snapdragon 460 (11 nm)"
  - id: "gpu"
    value: "Adreno 610"
  - id: "rom"
    value: "64GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "Android 10.0"
  - id: "battery"
    value: "5000 mAh"
  - id: "display"
    value: "720 x 1600 pixels, 20:9 ratio (~269 ppi density)"
  - id: "rearCamera"
    value: "13 MP, f/2.2, (wide), PDAF 2 MP, f/2.4, (macro) 2 MP, f/2.4, (depth)"
  - id: "frontCamera"
    value: "8 MP, f/2.0"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "164.9 x 75.1 x 8.5 mm (6.49 x 2.96 x 0.33 in)"
  - id: "weight"
    value: "188 g (6.63 oz)"
  - id: "releaseDate"
    value: "Nov 11 2020"

sources:
  portType: "community"
  portPath: "android10"
  deviceGroup: "oneplus-nord-n100"
  deviceSource: "oneplus-billie2"
  kernelSource: "kernel-oneplus-sm4250"

communityHelp:
  - name: "Telegram - @rubencarneiro"
    link: "https://t.me/rubencarneiro"

contributors:
  - name: "Rúben Carneiro"
    role: "Maintainer"
    forum: "https://gitlab.com/rubencarneiro"
    photo: "https://gitlab.com/uploads/-/system/user/avatar/2386810/avatar.png"
---
